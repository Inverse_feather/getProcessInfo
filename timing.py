# encoding = utf-8
import time
import tkinter
from email.header import Header
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
from apscheduler.schedulers.background import BackgroundScheduler


scheduler = BackgroundScheduler(timezone='Asia/Shanghai')

@scheduler.scheduled_job("cron", hour=9, minute=00)
def sendEmail():
    subject_content = 'windows主机防护软件长跑日报'
    mail_host = "smtp.exmail.qq.com"
    mail_sender = "zhangjs@tdhxkj.com"
    mail_license = "Tdhx2017.."
    mail_receivers = "xiehouxiayu@163.com,506385822@qq.com"
    mm = MIMEMultipart('related')
    mm["From"] = mail_sender
    mm["To"] = mail_receivers
    mm["Subject"] = Header(subject_content, 'utf-8')
    with open('outputMessage.txt', 'r') as f:
        body_content = f.read()
    message_text = MIMEText(body_content, "plain", "utf-8")
    mm.attach(message_text)
    stp = smtplib.SMTP_SSL(mail_host, 465)
    stp.login(mail_sender, mail_license)
    stp.sendmail(mail_sender, mail_receivers, mm.as_string())
    print("邮件发送成功")
    stp.quit()

if __name__ == '__main__':

    try:
        scheduler.start()
        while True:
            time.sleep(8)
    except (KeyboardInterrupt, SystemExit):
        scheduler.shutdown()
        print("程序退出成功！")
