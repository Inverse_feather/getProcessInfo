import socket
from time import sleep
import configparser

def connectServer():
    config = configparser.ConfigParser()
    config.read('processinfoconf.ini', encoding='utf-8')
    address = config.get('connect', 'addr')
    port = int(config.get('connect', 'port'))
    buffsize = 1024
    serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serv.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, True)
    serv.ioctl(socket.SIO_KEEPALIVE_VALS, (1, 60*1000, 30*1000))
    try:
        serv.connect((address, port))
        while True:
            senddata = "f"
            serv.send(senddata.encode())
            recvdata = serv.recv(buffsize).decode('utf-8')
            print(recvdata)

    except(ConnectionRefusedError, OSError):
        serv.close()

if __name__ == '__main__':
    connectServer()
